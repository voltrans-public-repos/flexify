#!/bin/bash
export FLEXIFY_SINGLE_ENGINE=true
export FLEXIFY_M_VERSION=2.8.1
export FLEXIFY_ENGINE_PASSKEY=R1VdrxNUiCf3ctl3E3Aj1i1CT8Gg2lmtm9/gqqHtZ0A=
export FLEXIFY_ENGINE_IP_MASK=172.16.0.0/12

set -v
set -e

# Install Docker
if ! [ -x "$(command -v docker)" ]; then
    echo 'docker is not installed. Installing...'
    curl -fsSL get.docker.com -o get-docker.sh
    sudo sh get-docker.sh
fi

# Install docker compose
if ! [ -x "$(command -v docker-compose)" ]; then
    echo 'docker-compose is not installed. Installing...'
    sudo curl -L https://github.com/docker/compose/releases/download/1.24.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
fi

# Downloads
# curl -O http://flexifyengine.blob.core.windows.net/docker/$FLEXIFY_M_VERSION/install.sh
# curl -O http://flexifyengine.blob.core.windows.net/docker/$FLEXIFY_M_VERSION/docker-compose.yml
# curl -O http://flexifyengine.blob.core.windows.net/docker/$FLEXIFY_M_VERSION/nginx/management-server.conf

# if [ "$FLEXIFY_SINGLE_ENGINE" == true ]; then
#     curl -O http://flexifyengine.blob.core.windows.net/docker/$FLEXIFY_M_VERSION/docker-compose.override.yml
#     curl -O http://flexifyengine.blob.core.windows.net/docker/$FLEXIFY_M_VERSION/nginx/engine.conf
#     curl -O http://flexifyengine.blob.core.windows.net/docker/$FLEXIFY_M_VERSION/nginx/engine-redirect.conf
#     curl -O http://flexifyengine.blob.core.windows.net/docker/$FLEXIFY_M_VERSION/nginx/engine-location.conf
# fi

# Directories
FLEXIFY_DIR=$(pwd)
mkdir -p $FLEXIFY_DIR/logs
mkdir -p $FLEXIFY_DIR/ssl

# Log in to repoistory
# docker login flexify.azurecr.io --username $FLEXIFY_REGISTRY_USERNAME --password $FLEXIFY_REGISTRY_PASSWORD

# Create SQL database
docker run --rm -d  \
    --name tmp-sql \
    -e MSSQL_PID=Express \
    -e ACCEPT_EULA=Y \
    -e 'MSSQL_SA_PASSWORD=Flexify#IO1' \
    -v flexify-sql:/var/opt/mssql \
    mcr.microsoft.com/mssql/server:2017-latest-ubuntu

# Several retries might be required to allow SQL served DB to start
retries=10
while [ $retries -gt 0 ]; do
    docker exec -it tmp-sql /opt/mssql-tools/bin/sqlcmd \
    -S localhost -U SA -P 'Flexify#IO1' \
    -Q 'CREATE DATABASE flexify
        GO
        ALTER DATABASE flexify SET ALLOW_SNAPSHOT_ISOLATION ON
        ALTER DATABASE flexify SET READ_COMMITTED_SNAPSHOT ON
        GO' &&
    break

    echo "Cannot create flexify db, let's wait 10 seconds and retry"
    sleep 10
    let retries=retries-1
done
if [ $retries -eq 0 ]; then
    echo "Unable to create flexify db after all retries!"
    docker stop tmp-sql
    exit 1
fi

docker stop tmp-sql

# Generate default certificates
# openssl req -x509 -nodes -days 3650 -newkey rsa:2048 \
#     -subj '/CN=localhost' \
#     -keyout $FLEXIFY_DIR/ssl/key.pem \
#     -out $FLEXIFY_DIR/ssl/cert.pem

# Create web console conf file
cat <<EOF > $FLEXIFY_DIR/conf.json
{
    "backendRootUrl": "/flexify-io/manage/api",
    "whitelistedDomains": [],
    "singleUser": true,
    "singleEngine": $FLEXIFY_SINGLE_ENGINE
}
EOF

# Additional config for single engine mode
if [ "$FLEXIFY_SINGLE_ENGINE" == true ]; then
    # Saving core dumps to logs folder
    echo "logs/core" > /proc/sys/kernel/core_pattern

    # Hourly logs cleanup - note that run-scripts will not process *.sh files
    cat <<EOF >/etc/cron.hourly/flexify-docker-cleanup-logs
    #!/bin/bash
    docker run --rm \
        -v $FLEXIFY_DIR/logs:/engine/logs \
        flexify.azurecr.io/engine:$FLEXIFY_E_VERSION \
        /engine/cleanup-logs.sh
EOF
    chmod +x /etc/cron.hourly/flexify-docker-cleanup-logs
fi

# Message of the Day
cat <<EOF > /etc/update-motd.d/99-flexify
#!/bin/sh
theIPaddress=\$(ip addr show eth0 | grep "inet\b" | awk '{print \$2}' | cut -d/ -f1)
echo "Flexify.IO multi-cloud storage and migration is installed on this machine."
echo " * Management console: http://\$theIPaddress"
echo " * More infromation: https://flexify.io"
echo
EOF
chmod 755 /etc/update-motd.d/99-flexify

# Run containers
docker-compose up -d
